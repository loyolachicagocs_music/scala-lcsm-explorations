name := "scala-lcsm"

version := "1.0"

resolvers += "laufer@bintray" at "http://dl.bintray.com/laufer/maven"

scalacOptions ++= Seq("-deprecation", "-feature", "-unchecked")

libraryDependencies ++= Seq("org.scalaz" %% "scalaz-core" % "7.0.5",
  "edu.luc.etl" %% "scalamu" % "0.2.2",
  "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test",
  "org.scalacheck" %% "scalacheck" % "1.11.1" % "test")
