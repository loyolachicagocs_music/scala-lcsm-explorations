package scala_lcsm

object ScalaStructures {

  sealed trait Symbol

  sealed trait Leaf extends Symbol { var duration: (Int, Int) = (1,4) }
  case class Note(pitch: Int) extends Leaf { require(0 <= pitch && pitch <= 127, "Pitch must fit with MIDI specs.")}
  case class Chord(pitches: Int*) extends Leaf //TODO - Scala: Enforce MIDI specs on List
  case class Rest() extends Leaf

  sealed trait Container extends Symbol
  case class Tuplet(multiplier: (Int,Int), music: Symbol*) extends Container
  case class Measure(timeSig: (Int,Int), music: Symbol*) extends Container
  case class Voice(music: Symbol*) extends Container

}
