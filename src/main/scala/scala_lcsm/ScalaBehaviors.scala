package scala_lcsm

import scala_lcsm.ScalaStructures._

object ScalaBehaviors {

  def guido(text: String): Symbol = {
    import scala.util.Random

    val notes = text.foldLeft(List.empty[Option[Symbol]]) {
      (list, next) => list :+ (next match {
        case 'a' | 'A' => Some(Note((43 :: 52 :: 60 :: Nil)(Random.nextInt(3)))) //G3 | E4 | C5
        case 'e' | 'E' => Some(Note((45 :: 53 :: 62 :: Nil)(Random.nextInt(3)))) //A3 | F4 | G4
        case 'i' | 'I' => Some(Note((47 :: 55 :: 64 :: Nil)(Random.nextInt(3)))) //B3 | G4 | E5
        case 'o' | 'O' => Some(Note((48 :: 57 :: 65 :: Nil)(Random.nextInt(3)))) //C4 | A4 | F5
        case 'u' | 'U' => Some(Note((50 :: 59 :: 67 :: Nil)(Random.nextInt(3)))) //D4 | B4 | G5
        case _ => None
      })
    }

    Voice(notes.flatten:_*)
  }

  def transpose(mod: Int, in: Symbol): Symbol = in match {
    case Note(pitch) => Note(pitch + mod)
    case Chord(pitches @ _*) => Chord(pitches map { _ + mod}:_*)
    case Voice(music @ _*) => Voice(music map { transpose(mod, _) }:_*)
    case Tuplet(m, music @ _*) => Tuplet(m, music map { transpose(mod, _) }:_*)
    case Measure(ts, music @ _*) => Measure(ts, music map { transpose(mod, _) }:_*)
    case Rest() => Rest()
  }
}
