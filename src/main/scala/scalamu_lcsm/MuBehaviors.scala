package scalamu_lcsm

import scalamu._
import MuStructures.SymbolFactory._
import scalamu_lcsm.MuStructures._
import scala.util.Random

object MuBehaviors {

  def transpose(steps: Int): Algebra[SymbolF, Symbol] = {
    case Note(pitch) => note(pitch + steps)
    case Chord(pitches @ _*) => chord(pitches map { _ + steps}:_*)
    case Rest() => rest()
    case Tuplet(m, s @ _*) => tuplet(m, s:_*)
    case Measure(ts, s @ _*) => measure(ts, s: _*)
    case Voice(s @ _*) => voice(s: _*)
  }

  //TODO - Mu: Maybe this can be written as a coalgebra?
  def guido(text: String): Symbol = {
    val notes = text.foldLeft(List.empty[Option[Symbol]]) {
      (list, next) => list :+ (next match {
        case 'a' | 'A' => Some(note((43 :: 52 :: 60 :: Nil)(Random.nextInt(3)))) //G3 | E4 | C5
        case 'e' | 'E' => Some(note((45 :: 53 :: 62 :: Nil)(Random.nextInt(3)))) //A3 | F4 | G4
        case 'i' | 'I' => Some(note((47 :: 55 :: 64 :: Nil)(Random.nextInt(3)))) //B3 | G4 | E5
        case 'o' | 'O' => Some(note((48 :: 57 :: 65 :: Nil)(Random.nextInt(3)))) //C4 | A4 | F5
        case 'u' | 'U' => Some(note((50 :: 59 :: 67 :: Nil)(Random.nextInt(3)))) //D4 | B4 | G5
        case _ => None
      })
    }

    voice(notes.flatten:_*)
  }

}
