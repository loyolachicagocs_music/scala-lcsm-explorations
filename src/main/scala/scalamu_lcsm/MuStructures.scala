package scalamu_lcsm

import scalaz.{ Equal, Functor, Show }
import scalamu._

object MuStructures {

  sealed trait SymbolF[+A]

  sealed trait Leaf extends SymbolF[Nothing] { var duration: (Int, Int) = (1, 4) }
  case class Note(pitch: Int) extends Leaf { require(0 <= pitch && pitch <= 127, "Pitch must fit MIDI specs") }
  case class Chord(pitch: Int*) extends Leaf //TODO - Mu: Chord pitch requirements
  case class Rest() extends Leaf

  sealed trait Container[A] extends SymbolF[A]
  case class Tuplet[A](multiplier: (Int,Int), symbols: A*) extends Container[A]
  case class Measure[A](timeSig: (Int,Int), symbols: A*) extends Container[A]
  case class Voice[A](symbols: A*) extends Container[A]

  implicit object SymbolFunctor extends Functor[SymbolF] {
    def map[A,B](fa: SymbolF[A])(f: A => B): SymbolF[B] = fa match {
      case Note(pitch) => Note(pitch)
      case Chord(pitches @ _*) => Chord(pitches:_*)
      case Rest() => Rest()
      case Tuplet(ts, c @ _*) => Tuplet(ts, c map f: _*)
      case Measure(ts, c @ _*) => Measure(ts, c map f: _*)
      case Voice(c @ _*) => Voice(c map f: _*)
    }
  }

  implicit def SymbolFEqual[A]: Equal[SymbolF[A]] = Equal.equalA
  implicit def SymbolFShow[A]: Show[SymbolF[A]] = Show.showFromToString
  type Symbol = µ[SymbolF]

  object SymbolFactory {
    def note(pitch: Int): Symbol = In(Note(pitch))
    def chord(pitches: Int*): Symbol = In(Chord(pitches:_*))
    def rest(): Symbol = In(Rest())
    def tuplet(m: (Int,Int), symbols: Symbol*): Symbol = In(Tuplet(m, symbols:_*))
    def measure(ts: (Int,Int), symbols: Symbol*): Symbol = In(Measure(ts, symbols:_*))
    def voice(symbols: Symbol*): Symbol = In(Voice(symbols:_*))
  }

}
