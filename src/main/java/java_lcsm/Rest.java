package java_lcsm;

/**
 * Leaf implementation that represents a musical rest.
 */
public class Rest extends Leaf {

    public Rest(int durationNum, int durationDenom) {
        super(durationNum, durationDenom);
    }

}

