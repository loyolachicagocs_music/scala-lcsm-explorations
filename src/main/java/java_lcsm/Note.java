package java_lcsm;

/**
 * Leaf implementation that represents a musical note.
 */
public class Note extends Leaf {

    private int pitch;

    public Note(int pitch, int durationNum, int durationDenom) {
        super(durationNum, durationDenom);

        this.pitch = pitch;
    }

    public int getPitch() {
        return pitch;
    }

}
