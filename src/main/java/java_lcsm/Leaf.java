package java_lcsm;

/**
 * This class represents "leaf" elements in the LCSM tree. This includes things like notes, chords, and rests.
 */
public abstract class Leaf {

    int durationNum;
    int durationDenom;

    public Leaf(int durationNum, int durationDenom) {
        this.durationNum = durationNum;
        this.durationDenom = durationDenom;
    }

    /**
     * The numerator of the Leaf duration.
     *
     * This is the length of the duration: (one, two, three) eighth note(s).
     * @return Duration numerator.
     */
    public int getDurationNum() {
        return durationNum;
    }

    /**
     * The denominator of the Leaf duration.
     *
     * This is the time scale of the duration: one (quarter, eighth, etc.) note.
     * @return Duration denominator.
     */
    public int getDurationDenom() {
        return durationDenom;
    }
}
