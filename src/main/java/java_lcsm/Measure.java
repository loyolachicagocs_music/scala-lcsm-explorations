package java_lcsm;

import java.util.List;

/**
 * Container implementation that represents a measure containing notes.
 */
public class Measure extends Container {

    private int timeSigNum;
    private int timeSigDenom;

    public Measure() {
        super();
    }

    public Measure(int timeSigNum, int timeSigDenom, List<Leaf> leaves) {
        super(leaves);
        this.timeSigNum = timeSigNum;
        this.timeSigDenom = timeSigDenom;
    }

    public int getTimeSigNum() {
        return timeSigNum;
    }

    public int getTimeSigDenom() {
        return timeSigDenom;
    }
}
