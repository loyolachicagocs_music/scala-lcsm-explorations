package java_lcsm;

/**
 * This class represents "mark" elements in the LCSM tree. These are "leaf" decorators.
 */
public abstract class Mark { }
