package java_lcsm;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the "container" element of the LCSM tree. This includes staves, measures, and tuples.
 */
public abstract class Container {

    private List<Leaf> leaves = new ArrayList<Leaf>();

    public Container() {  }

    public Container(List<Leaf> leaves) {
        this.leaves = leaves;
    }

    public List<Leaf> getLeaves() {
        return leaves;
    }

    public boolean append(Leaf leaf) {
        return leaves.add(leaf);
    }

    public boolean append(List<Leaf> leaves) {
        return leaves.addAll(leaves);
    }

}
