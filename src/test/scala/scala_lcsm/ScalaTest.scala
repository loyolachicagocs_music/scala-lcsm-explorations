package scala_lcsm

import scala_lcsm.ScalaBehaviors._
import scala_lcsm.ScalaStructures._
import org.scalatest.FunSuite

class ScalaTest extends FunSuite {

  info("Guido: "+guido("I love coffee"))

  test("Transposition - Note") {
    assert(transpose(12, Note(36)) == Note(48))
  }

  test("Transposition - Measure") {
    assert(transpose(12, Measure((4,4), Note(12), Note(14), Note(16))) == Measure((4,4), Note(24), Note(26), Note(28)))
  }
}
