package scalamu_lcsm

import org.scalatest.FunSuite
import scalaz.syntax.equal._
import scalamu._

import MuStructures.SymbolFactory._
import MuBehaviors._

class MuTest extends FunSuite {

  info("Guido: "+guido("I love coffee"))

  test("Transposition - Note") {
    note(48) cata transpose(12) assert_=== note(60)
  }

  test("Transposition - Measure") {
    measure((4,4), note(48), note(52), note(55), note(60)) cata transpose(12) assert_===
      measure((4,4), note(60), note(64), note(67), note(72))
  }

}
