Introduction
============

.. _LCSM: http://abjad.mbrsi.org/system_overview/lcs/index.html
.. _Scalaz: https://github.com/scalaz/scalaz
.. _Scalaµ: https://bitbucket.org/lucproglangcourse/scalamu
.. _here: http://musimat.com/MusimatChapter9/html/_c090400_8cpp.html

The premise of this project is to represent Abjad's `LCSM`_ music notation model within Scala.
Our goal is to be able to represent and manipulate music notation in a type-safe way, then 
generating Python/Abjad code from it. To this end, we are experimenting with different 
implementations of the LCSM model to find out the pros and cons of each. There are four 
separate implementations: Java, pure Scala, Scala with `Scalaz`_, and Scala with
`Scalaµ`_.

Each of these LCSM variants implement three Leaves (Note, Rest, Chord) and three Containers
(Tuplet, Measure, Voice). Each implementation will have one composition function that uses a
stochastic version of Guido's method (outlined `here`_) to generate structures, and another
function that transposes existing structures.

Java Implementation
===================

Attempting to implement LCSM in Java was more of an academic exercise than anything else. Normally
I don't mind the amount of boilerplate Java requires, but for something like LCSM it becomes very 
tedious to add new classes and functionality. OOP does not lend itself nicely to this model, for 
example all *leaf* nodes will have duration but *notes* have pitch, *chords* have multiple 
pitches, and *rests* have none. This variance in functionality inherently causes reliance on 
concrete classes, which gets even worse when LCSM's recursion in factored in. Python's dynamic
typing is a surprising ally when implementing this model!

*For now, this implementation has been left unfinished.*

Scala Implementation
====================

Much better than the Java implementation! The boilerplate that Java requires has completely disappeared:
each class is now a simple one-liner including pitch value restrictions and note duration. The Guido algorithm
that took about 23 lines in Python now takes 11 lines (with possible room for improvement!). The only part
that got messy was writing a transposition method, which required some hard-to-follow code. Some of this
can be improved by having Containers accept Lists as opposed to arbitrary argument lists, and it is also
something that I expect Scalaz/µ can handle with ease.

Scalaz Implementation
=====================

*TODO*

Scalaµ Implementation
=====================

The Scalaµ data structure is very similar to that of the Scala implementation with the addition of the
Endofunctor. Transposition becomes slightly simplified thanks to catamorphisms, but the Guido algorithm
appears to be too complicated to describe as a coalgebra. Even if I am wrong about this, if we encounter
algorithms that are too complex to describe as F-algebras I see no need to use Scalaµ.

*If someone can re-write the Guido algorithm using a coalgebra, it would be greatly appreciated!*
